# How To Create A GNOME Shell Extension Documentation

This article is a textual document for this [video series](https://www.youtube.com/playlist?list=PLr3kuDAFECjZhW-p56BoVB7SubdUHBVQT). The video series can help you to understand the examples better.

Other than MD file, you can read the documentation in [GitLab Page](http://justperfection.channel.gitlab.io/how-to-create-a-gnome-extension-documentation/).

## License

- Examples have been released under The **MIT License**.

- The article is released under **CC0 1.0 Universal (CC0 1.0)** License.

```
You can copy, modify, distribute and perform the work,
even for commercial purposes, all without asking permission.

- In no way are the patent or trademark rights of any person affected by CC0,
nor are the rights that other persons may have in the work or in how
the work is used, such as publicity or privacy rights.

- Unless expressly stated otherwise, the person who associated a work with
this deed makes no warranties about the work, and disclaims liability
for all uses of the work, to the fullest extent permitted by applicable law.

- When using or citing the work, you should not imply endorsement by
the author or the affirmer.
```

*Disclaimer: I’m not a GNOME developer. I just did this article to help the community.*
